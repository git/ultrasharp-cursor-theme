# ultrasharp-cursor-theme: Precision and speed oriented cursor theme

This is a precision and speed oriented cursor theme, the hotspot is known, contrast is optimised, pixels are perfect. Roughly the opposite of pastel butter-knife looking cursor themes.

## Specifications
- [XDG cursor-spec](https://www.freedesktop.org/wiki/Specifications/cursor-spec/)

## Dependencies
- ninja / samurai
- POSIX Shell
- [quoter](https://github.com/vaeth/quoter/)
- [xcursorgen](https://gitlab.freedesktop.org/xorg/app/xcursorgen)
- `magick(1)` command (ImageMagick, GraphicsMagick, …)

## Supported environments
- XOrg (deprecated)
- wlroots-based compositors

## Building
- `./configure`
- You can use any of `PREFIX`, `DATADIR`, `ICONDIR` to change the path.

## Installing
- `ninja install`
- For packaging, `DESTDIR` is supported

## Thanks
(LibreSprite)[https://libresprite.github.io/] (and GPLv2 Aseprite Authors) for an image editor which nicely allows to manipulate pixels as RGBA.
